package model;

public class NestedLoop {
	
	public String nested_loop1(int n){
		String str = "";
		for(int i = 1; i<=n-1; i++){
			for(int j = 1; j<=n; j++){
				str += "*";
			}
			str += "\n";
		}
		return str;
	}
	
	public String nested_loop2(int n){
		String str = "";
		for(int i = 1; i<=n; i++){
			for(int j = 1; j<=n-1; j++){
				str += "*";
			}
			str += "\n";
		}
		return str;
	}
	
	public String nested_loop3(int n){
		String str = "";
		for(int i = 1; i<=n; i++){
			for(int j = 1; j<=i; j++){
				str += "*";
			}
			str += "\n";
		}
		return str;
	}
	
	public String nested_loop4(int n){
		String str = "";
		for(int i = 1; i<=n; i++){
			for(int j = 1; j<=n+2; j++){
				if(j%2 == 0){
					str += "*";
				}else{
					str += "-";
				}
				
			}
			str += "\n";
		}
		return str;
	}
	public String nested_loop5(int n){
		String str = "";
		for(int i = 1; i<=n; i++){
			for(int j = 1; j<=n+2; j++){
				if((i+j)%2 == 0){
					str += "*";
				}else{
					str += " ";
				}
			}
			str += "\n";
		}
		return str;
	}
	

}

package view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import controller.Control;

public class View extends JFrame {
	private static final int FRAME_WIDTH = 500;
	private static final int FRAME_HEIGHT = 300;
	private Control control = new Control();

	private JPanel panel1;
	private JPanel panel2;
	private JComboBox<String> combobox;
	private JTextArea area;
	private JButton button;
	private JTextField field;
	private JLabel label;

	public static void main(String[] args) {
		View v = new View();
	}

	public View() {
		setLayout(new GridLayout(1, 2));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		area = new JTextArea(10, 20);
		button = new JButton("Submit");
		button.addActionListener(new AddInterestListener());

		panel1 = new JPanel(new GridLayout(8, 1));
		String labels[] = { "output1", "output2", "output3", "output4",
				"output5" };
		combobox = new JComboBox<String>(labels);
		field = new JTextField("1");
		label = new JLabel("input num");
		panel1.add(combobox);
		panel1.add(label);
		panel1.add(field);
		panel1.add(button);

		panel2 = new JPanel();
		panel2.add(area);

		add(panel1);
		add(panel2);

		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		setVisible(true);
	}

	class AddInterestListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			int n = Integer.parseInt(field.getText());
			if (combobox.getSelectedItem().toString().equals("output1")) {
				String s = control.callNestedLoop1(n);
				area.setText(s);
			}else if (combobox.getSelectedItem().toString().equals("output2")) {
				String s = control.callNestedLoop2(n);
				area.setText(s);
			}
			else if (combobox.getSelectedItem().toString().equals("output3")) {
				String s = control.callNestedLoop3(n);
				area.setText(s);
			}
			else if (combobox.getSelectedItem().toString().equals("output4")) {
				String s = control.callNestedLoop4(n);
				area.setText(s);
			}else{
				String s = control.callNestedLoop5(n);
				area.setText(s);
			}

		}
	}

}

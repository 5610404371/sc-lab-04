package controller;

import model.NestedLoop;

public class Control {
	NestedLoop nl = new NestedLoop();
	public String callNestedLoop1(int n){
		return nl.nested_loop1(n);
	}
	public String callNestedLoop2(int n){
		return nl.nested_loop2(n);
	}
	public String callNestedLoop3(int n){
		return nl.nested_loop3(n);
	}
	public String callNestedLoop4(int n){
		return nl.nested_loop4(n);
	}
	public String callNestedLoop5(int n){
		return nl.nested_loop5(n);
	}
	
}
